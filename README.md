# delivery

> Проект на котором нужно попробовать как можно больше

# Что используется в проекте
 - vue
 - vuex
 - vue-route
 - ssr
 - vee-validate
 - v-mask
 - nuxt.js
 - express
 - scss

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
