export const state = () => ({
  basket: []
});
export const actions = {
  changeFood({state, commit}, {food, foodId, count, url}) {
    const foodIndex = state.basket.findIndex(({id}) => id === foodId);

    if (foodIndex + 1) {
      if (count > 0) {
        commit('addFood', {foodIndex, count});
      } else {
        const foods = state.basket.filter(({id}) => id !== foodId);

        commit('removeFood', foods);
      }
    } else {
      if (count > 0) {
        commit('setFood', {food, id: foodId, count, url});
      }
    }
  },

  changeFoodCount({state, commit}, {foodId, count}) {
    const foodIndex = state.basket.findIndex(({id}) => id === foodId);

    if (count > 0) {
      commit('changeFoodCount', {foodIndex, count})
    } else {
      const foods = state.basket.filter(({id}) => id !== foodId);

      commit('removeFood', foods);
    }


  }
};

export const mutations = {
  setFood(state, foodObject) {
    state.basket.push(foodObject)
  },
  clear(state) {
    state.basket = [];
  },
  removeFood(state, foods) {
    state.basket = foods;
  },
  changeFoodCount(state, {foodIndex, count}) {
    state.basket[foodIndex].count = count
  },
  addFood(state, {foodIndex, count}) {
    state.basket[foodIndex].count += count
  }
};

export const getters = {
  getBasket: s => s.basket,
  getBasketForOrder: s => s.basket.map(({id, count}) => ({id, count})),
  getBasketCount: s => s.basket.reduce((sum, {count}) => sum + count, 0),
  getBasketSum: s => s.basket.reduce((sum, {count, food}) => sum + (count * food.price), 0)
}
