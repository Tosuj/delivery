const { Schema, model } = require('mongoose')
const sc2 = new Schema({
  address: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  orderItems: [Schema.Types.Mixed]

})
module.exports = model('Order', sc2)
