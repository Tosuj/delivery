const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express()
const bodyParser = require('body-parser')
app.use(
  bodyParser.urlencoded({
    extended: true
  })
)
app.use(bodyParser.json())
const menuRoutes = require('../routes/menus')
const mongoose = require('mongoose')
const Pizza = require('../models/Pizza')
const Roll = require('../models/Roll')
const Drink = require('../models/Drink')
const Order = require('../models/Order')
// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'


app.get('/getMenu', async function(req, res) {
    switch (+req.query.m) {
      case 1: {
        const pizzas = await Pizza.find({});
        await res.json(pizzas);
        break
      }
      case 2: {
        const pizzas = await Roll.find({});
        await res.json(pizzas);
        break
      }
      case 3: {
        const pizzas = await Drink.find({});
        await res.json(pizzas);
        break
      }
      default :
        res.send('no')

    }
});

app.post('/makeorder', async function(req, res) {
  const order = new Order({
    phone: req.body.phone,
    address: req.body.address,
    orderItems: req.body.order
  })

  await order.save()
  res.send('OK')

})


async function start () {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  await nuxt.ready()
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)
  app.use(menuRoutes)

  await mongoose.connect(
    'mongodb+srv://george:qwertyfuck15@cluster0-bi0ip.mongodb.net/todos',
    {
      useNewUrlParser: true,
      useFindAndModify: false
    })
  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
